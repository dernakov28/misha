const net = require('net');
const WebSocket  = require('ws');

const RESPONSE = {
	status: 1
};

const wss = new WebSocket.Server({
	port: 6002
});

wss.broadcast = function broadcast(data) {
	wss.clients.forEach(function each(client) {
		if (client.readyState === WebSocket.OPEN) {
			client.send(data);
		}
	});
};

const server = net.createServer();

server.on('connection', socket => {
	wss.broadcast('Connected.')

	socket.on('data', buffer => {
		const message = buffer.toString('utf-8');

		wss.broadcast(message);

		try {
			JSON.parse(message);
			wss.broadcast('Ok.')

			socket.write(JSON.stringify(RESPONSE));
		} catch (err) {
			wss.broadcast('Error! Json is invalid.')
		}
	});

	socket.on('close', _ => {
		wss.broadcast('Disconnected.')
	});
});

server.listen(6001);
